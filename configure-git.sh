#!/bin/sh
# Call with configure-git.sh "Username" "Emailaddress"
git config --global user.name "$1"
git config --global user.email "$2"
git config --global core.autocrlf input
if [ -x "$(command -v nvim)" ]; then
git config --global core.editor nvim
elif [ -x "$(command -v vim)" ]; then
git config --global core.editor vim
fi
git config --global alias.sac "!git stash apply; git stash clear"

gpg_id=$(gpg --list-secret-keys --keyid-format LONG | grep sec | grep -Eo '(/[A-Z0-9]*)\s' | cut -d "/" -f 2)
if [ "$gpg_id" ]; then
	git config --global commit.gpgsign true
	git config --global user.signingkey $gpg_id
fi
echo '{"path": "cz-conventional-changelog"}' > ~/.czrc

