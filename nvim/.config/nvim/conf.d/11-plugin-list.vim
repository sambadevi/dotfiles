
call plug#begin('~/.config/nvim/plugged')

Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'

Plug 'vim-scripts/tComment'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'tpope/vim-surround'

Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

Plug 'junegunn/goyo.vim'

Plug 'ap/vim-css-color'
Plug 'lepture/vim-jinja'
Plug 'posva/vim-vue'
Plug 'kovetskiy/sxhkd-vim'
Plug 'fatih/vim-go'

Plug 'majutsushi/tagbar'

if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
let g:deoplete#enable_at_startup = 1

Plug 'Shougo/neosnippet.vim'
Plug 'Shougo/neosnippet-snippets'
Plug 'deoplete-plugins/deoplete-go', { 'do': 'make'}
call plug#end()
