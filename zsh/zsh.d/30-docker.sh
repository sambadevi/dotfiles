#!/bin/bash

if [ -x "$(command -v docker)" ]; then
dkb(){
  docker build -t $(whoami)/$(basename $PWD) .
}
dkr(){
  eval "docker run --rm -it $@ --name $(basename $PWD) $(whoami)/$(basename $PWD)"
}
dkrd(){
  eval "docker run --rm -it -d $@ --name $(basename $PWD) $(whoami)/$(basename $PWD)"
}

dkrma(){
  docker stop $(docker ps -q)
  docker rm $(docker ps -qa)
}
fi
