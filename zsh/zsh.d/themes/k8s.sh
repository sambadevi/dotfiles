#!/bin/zsh
USER_BLOCK='%B%F{173}%n%f%b'
STATUS_BLOCK='%(?.%F{112} %f.%F{196} %f)'
STATUS_BLOCK_R='%B%(?.%F{112}√%f.%F{196}%?%f)%b'
TIME_BLOCK='%*'
DIR_BLOCK='%B%F{blue}%2~%f%b'

RPROMPT='${STATUS_BLOCK_R}%B%F{1}$(current_k8s_context)%f%b${TIME_BLOCK}'
PROMPT=$'\n${STATUS_BLOCK}${USER_BLOCK} ${DIR_BLOCK} %B%F{cyan}\$vcs_info_msg_0_%f%b %# '

autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst

zstyle ':vcs_info:git:*' formats " %b%c%u%m"
zstyle ':vcs_info:git:*' actionformats " %b%c%u%m|%a"
zstyle ':vcs_info:git:*' unstagedstr ' '
zstyle ':vcs_info:git:*' stagedstr ' 落'
zstyle ':vcs_info:git:*' check-for-changes 'true'
zstyle ':vcs_info:git:*' check-for-unstaged-changes 'true'
zstyle ':vcs_info:git*+set-message:*' hooks git-st

