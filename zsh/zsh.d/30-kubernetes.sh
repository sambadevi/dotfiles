#!/bin/bash
if [ -x "$(command -v az)" ]; then
  kubedash(){
    az aks browse --name "${K8S_AZ_RESOURCE_NAME}" --resource-group "${K8S_AZ_RESOURCE_GROUP}"
  }
if [ -x "$(command -v xpanes)" ]; then
  kshow(){
    if [ -z "$1" ]; then
      echo "Nothing to search for!" >&2
      return 1
    fi
    if [ -n "$2" ]; then
      types=($2)
    else
      types=(ingress svc deploy pod)
    fi
    xpanes -sc "watch -n2 'hr;echo $(current_k8s_context) Showing {} resources;hr;kubectl get {} | grep $1'" $types[@]
  }
fi
if [ -x "$(command -v kubectx)" ]; then
  kubeswitch(){
    kubefile=~/.kubeswitch/${1}.kube
    if [ -e ${kubefile} ]; then
      subscription="$(grep 'subscription' ${kubefile} | awk '{ print $2; }')"
      group="$(grep 'group' ${kubefile} | awk '{ print $2; }')"
      resource="$(grep 'resource' ${kubefile} | awk '{ print $2; }')"
      az account set -s "${subscription}"
      K8S_AZ_RESOURCE_GROUP="${group}"
      K8S_AZ_RESOURCE_NAME="${resource}"
      echo "K8S_AZ_RESOURCE_GROUP='${K8S_AZ_RESOURCE_GROUP}'" > ~/.az-context
      echo "K8S_AZ_RESOURCE_NAME='${K8S_AZ_RESOURCE_NAME}'" >> ~/.az-context
      kubectx "${1}"
    else
      echo "Error: ${kubefile} not found!"
    fi
  }
fi
fi
