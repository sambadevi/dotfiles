#!/bin/bash
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_FIND_NO_DUPS
typeset -gA ZSH_HIGHLIGHT_STYLES
ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=white'
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]='fg=122'
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]='fg=112'
if [ -x "$(command -v nvim)" ]; then
  export EDITOR=nvim
elif [ -x "$(command -v vim)" ]; then
  export EDITOR=vim
elif [ -x "$(command -v vi)" ]; then
  export EDITOR=vi
fi
if [ -x "$(command -v firefox)" ]; then
  export BROWSER="$(command -v firefox)"
elif [ -x "$(command -v surf)" ]; then
  export BROWSER="$(command -v surf) -z 1.5"
fi
export PAGER=less
if [ -x "$(command -v coderay)" ]; then
  export FZF_CTRL_T_OPTS='--height 100% --preview "coderay {}"'
fi
export GPG_TTY=$(tty)
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin
export PATH=$HOME/.bin:$PATH
export PATH=$HOME/.local/bin:$PATH
export PATH=$HOME/.gem/ruby/2.5.0/bin:$PATH
export PATH=${GOPATH//://bin:}/bin:$PATH
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
export PATH=$PATH:$HOME/bin
if [ -x "$(command -v pass)" ]; then
  export PASSWORD_STORE_X_SELECTION=primary
fi
# load external env files from HOME
test -f ~/.env && source ~/.env
test -f ~/.zsh_env && source ~/.zsh_env
test -f ~/.zshenv && source ~/.zshenv
