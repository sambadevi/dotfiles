#!/bin/bash
if [ -x "$(command -v systemctl)" ]; then
  alias usys='systemctl --user'
fi
if [ -x "$(command -v journalctl)" ]; then
  alias ujourn='journalctl --user'
fi
