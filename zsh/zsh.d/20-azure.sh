#!/bin/bash
if [ -x "$(command -v az)" ]; then
  alias azacc="az account set -s"
fi
